import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { EnginesEffects } from './engines.effects';

describe('EnginesEffects', () => {
  let actions$: Observable<any>;
  let effects: EnginesEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        EnginesEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(EnginesEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
