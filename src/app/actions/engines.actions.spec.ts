import * as fromEngines from './engines.actions';

describe('[engines]Enginess', () => {
  it('should return an action', () => {
    expect(fromEngines.[engines]Enginess().type).toBe('[Engines] [engines] Enginess');
  });
});
