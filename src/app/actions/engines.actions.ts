import { createAction, props } from '@ngrx/store';

export const FetchEnginesRequest = createAction(
  '[Engines] fetch enginess request'
);

export const FetchEnginesSuccess = createAction(
  '[Engines] fetch enginess success',
  props<{ engines: any }>()
);

export const FetchEnginesFailure = createAction(
  '[Engines] fetch enginess failure',
  props<{ error: any }>()
);

