# RecTest

## Prerequisites

- [Node.js](https://nodejs.org) ≥ v10.x
- [npm](https://www.npmjs.com/package/npm) ≥ v6.x

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Task

- Fork this repository
- Create a service with a method to fetch the static data from `/assets/engines.json` and store the engine items into redux store under `engines` sorted by oem.
- Create a page to display the engine items fetched from the endpoint described in previous step. Use the `enginesSelector` to get the data from the redux store. Engine details (oem, name and list of engine models) should be displayed in the table format (by using e.g. flex-box or ideally css-grid) with following rules (see `screenshots/engines-grid.png`):
  - For screen size under `480px` - 1 column grid
  - For screen size larger then `480px` and smaller then `1024px` - 2 column grid
  - For screen size larger then `1024px` - 4 column grid
- Create a route in `app-routing.module` for the page created in previous step and add a link on the main page to the new page created in the previous step
- Create a merge request with your solution
